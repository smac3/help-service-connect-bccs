package com.example.helpservice.business;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.helpservice.database.dto.HelpDTO;
import com.example.helpservice.database.entity.Help;

@Service
public interface HelpBusiness {
	public List<HelpDTO> getAll();
    public Help update(Help help);
    public Help create(Help help);
    public int delete(int helpId);

}
