package com.example.helpservice.business.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.helpservice.business.HelpBusiness;
import com.example.helpservice.database.dto.HelpDTO;
import com.example.helpservice.database.entity.Help;
import com.example.helpservice.database.entity.Staff;
import com.example.helpservice.database.repo.HelpRepo;
import com.example.helpservice.database.repo.StaffRepo;

public class HelpBusinessImpl implements HelpBusiness {

	@Autowired
	private HelpRepo helpRepo;
	
	@Autowired
	private StaffRepo staffRepo;

	public List<HelpDTO> getAll() {
		List<Staff> staffs = staffRepo.findAll();
        List<HelpDTO> helpDtos = new ArrayList<>();
        for (Staff staff : staffs) {
            List<Help> helps = helpRepo.findByCreatedUser(staff.getStaffCode());
            for (Help help : helps) {
                HelpDTO helpDto = new HelpDTO();
                helpDto.setParentHelpId(help.getParentHelpId());
                helpDto.setShopId(staff.getShopId());
                helpDto.setStaffId(staff.getStaffId());
                helpDto.setStatus(help.getStatus());
                helpDto.setType(help.getType());
                helpDtos.add(helpDto);
            }
        }
        return helpDtos;
	}

	@Override
	public Help update(Help help) {
		Optional<Help> optionalHelp = helpRepo.findById(help.getHelpId());
        if (optionalHelp.isPresent()) {
            Help helpNew = optionalHelp.get();
            helpNew.setContent(help.getContent());
            helpNew.setCreatedUser(help.getCreatedUser());
            helpNew.setParentHelpId(help.getParentHelpId());
            helpNew.setHelpName(help.getHelpName());
            helpNew.setPosition(help.getPosition());
            helpNew.setStatus(help.getStatus());
            helpNew.setType(help.getType());
            return helpRepo.save(helpNew);
        }
        return null;
	}

	@Override
	public Help create(Help help) {
		return helpRepo.save(help);
	}

	@Override
	public int delete(int helpId) {
		List<Integer> helpIds = helpRepo.selectHelpIdLevel(helpId);
        for (Integer helpIdSub : helpIds) {
            helpRepo.delete(helpIdSub);
        }
        return 1;
	}

}
