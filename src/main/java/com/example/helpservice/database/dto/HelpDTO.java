package com.example.helpservice.database.dto;

import lombok.Data;

@Data
public class HelpDTO {

	    private Integer staffId;

	    private Integer parentHelpId;

	    private String status;

	    private String type;

	    private Integer shopId;

		public Integer getStaffId() {
			return staffId;
		}

		public void setStaffId(Integer staffId) {
			this.staffId = staffId;
		}

		public Integer getParentHelpId() {
			return parentHelpId;
		}

		public void setParentHelpId(Integer parentHelpId) {
			this.parentHelpId = parentHelpId;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public Integer getShopId() {
			return shopId;
		}

		public void setShopId(Integer shopId) {
			this.shopId = shopId;
		}

	

}
