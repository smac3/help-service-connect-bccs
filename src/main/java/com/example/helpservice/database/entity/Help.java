package com.example.helpservice.database.entity;


import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="tmp_help")
public class Help {

    @Id
    @Column(name = "help_id")
    private int helpId;

    @Column(name = "help_name")
    private String helpName;

    @Column(name = "parent_help_id")
    private Integer parentHelpId;

    @Column(name = "type")
    private String type;

    @Column(name = "position")
    private String position;

    @Column(name = "content")
    private String content;

    @Column(name = "status")
    private String status;

    @Column(name = "created_user")
    private String createdUser;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "staff_code")
    private Staff staff;

	public int getHelpId() {
		return helpId;
	}

	public void setHelpId(int helpId) {
		this.helpId = helpId;
	}

	public String getHelpName() {
		return helpName;
	}

	public void setHelpName(String helpName) {
		this.helpName = helpName;
	}

	public Integer getParentHelpId() {
		return parentHelpId;
	}

	public void setParentHelpId(Integer parentHelpId) {
		this.parentHelpId = parentHelpId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(String createdUser) {
		this.createdUser = createdUser;
	}

	public Staff getStaff() {
		return staff;
	}

	public void setStaff(Staff staff) {
		this.staff = staff;
	}
    
    

}

