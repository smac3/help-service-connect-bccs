package com.example.helpservice.database.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.helpservice.database.entity.Staff;

@Repository
public interface StaffRepo extends JpaRepository<Staff, Integer>{

}
